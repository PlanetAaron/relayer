#include <stdio.h>
#include <string.h>

@interface NCNotificationContent:NSObject
@property (nonatomic,copy,readonly) NSString * header;
@property (nonatomic,copy,readonly) NSString * title;
@property (nonatomic,copy,readonly) NSString * subtitle;
@property (nonatomic,copy,readonly) NSString * message;
@end

@interface NCNotificationRequest:NSObject
@property (nonatomic,readonly) NCNotificationContent * content;
@property (nonatomic,copy,readonly) NSDictionary * sourceInfo;
@end

int sendNotif(const char** title, const char** subtitle, const char** message)
{
	//TODO
	return 0;
}

%hook NCNotificationStore

-(id)addNotificationRequest:(id)notif
{
	NCNotificationRequest *req = notif;
	// Run this first to make sure the notification shows up on the device before we do anything
	id ret = %orig;

	NSString *nstitle = req.content.title;
	NSString *nssubtitle = req.content.subtitle;
	NSString *nsmessage = req.content.message;

	// Get Notification Content
	const char* title = [nstitle UTF8String];
	const char* subtitle = [nssubtitle UTF8String];
	const char* message = [nsmessage UTF8String];
	sendNotif(&title, &subtitle, &message);
	return ret;
}

%end
