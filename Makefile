include $(THEOS)/makefiles/common.mk

TWEAK_NAME = relayer
relayer_FILES = Tweak.xm
ARCHES = arm64 arm64e

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
